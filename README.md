# Neuronal Networks Server

## Pasos para instalacion y ejecucion del sistema:
**IMPORTANTE: Verificar que todas las máquinas (servidores y clientes) estén en la misma red para poder acceder al 
dataset.**

1. Instalación de python (versiones de Python que soporta: 3.10.6 y 3.7.9).
2. Instalación de las librerías *tensorflow* con pip.
3. Instalar la librería que necesita su proyecto.
4. Crear una carpeta para su proyecto, con el archivo tensorflow_server.py y todos los archivos necesarios en él.
   - Verificar el formato del archivo “tensorflow_server.py” en la carpeta “client_project”
5. Poner la carpeta del dataset y checkpoint como compartido en la red y verificar que no posea contraseña en el
   firewall.
6. Agregar la dirección (en la red) del dataset, el checkpoint y la dirección en el servidor en que se encuentra el 
   proyecto a ejecutarse al archivo de configuración.

## Formato del Archivo Tensorflow Server

El archivo *tensorflow_server.py* tiene una clase *TensorflowServer* en la que se deben inicializar
las variables y métodos para el entrenamiento, el innit de esta clase recibe un *cluster_conf* que contiene
los datos del cluster.
En este se debe compilar el modelo dentro de *with starategy.scope()* y hacer el fit para el entrenamiento

## Configuracion de correo

Para el funcionamiento de las notificaciones por correo es necesario crear una contraseña para aplicaciones en el gmail
que sera utilizado como emisor:

1. Activar la verificacion en dos pasos.
2. Una vez activada, ir a las opciones de verificacion de dos pasos.
3. Ir a Contraseñas de aplicaciones
4. Seleccionar app -> Correo electronico
5. Selecionar dispositivo -> Otra(Nombre personalizado)
6. Generar
7. Una vez generado la contraseña, copiar y pegarlo en:
   * config.json -> mailer -> password

## Archivo de configuración "*config.json*"
1. **broadcast**
   - **port:** puerto en el que el servidor realizará el broadcast.
   - **wait_time_seconds:** tiempo que esperará para realizar el archivo json que necesita enviarle a todos los clientes 
   que respondieron al broadcast.
2. **tensorflow**
   - **max_ps:** máximo de ps que creará el servidor, en caso de que haya menos clientes que ps entonces solo se creará 
   la cantidad de ps que clientes estén conectados.
   - **has_ps_workers:** en caso de que sea true, un cliente podrá ser ps y worker al mismo tiempo, en caso de que sea 
   falso un cliente solo podrá ocupar uno de los dos trabajos.
   - **designated_ps:** en caso de que existan ip’s asignará a estos clientes como ps; en caso de que las ip’s que 
   figuren en la lista no existan en la red entonces otorgará los ps de forma aleatoria entre los clientes; en caso de 
   que la lista esté vacía otorgará los ps de forma aleatoria entre los clientes.
3. **mailer**
   - **email:** correo desde el que se envían las notificaciones.
   - **password:** contraseña del correo desde el que se envían las notificaciones.
   - **receiver:** correo en el que se reciben las notificaciones.
   - **header:** cabecera del correo.
   - **smtp_server:** servidor del correo que utiliza.
   - **smtp_port:** puerto del correo que utiliza.
   - **enabled:** en caso de que sea false no enviará correos; en caso de que sea true enviará los correos.
   - **enabled_epoch:** en caso de que sea false no se notificará el inicio de los epoch; en caso de que sea true 
   enviará una notificación al inicio de cada epoch.
4. **paths**
   - **client_project_dir:** dirección donde se ubicá el proyecto del alumno.
   - **checkpoint_dir:** dirección del archivo compartido donde se guardará o recuperará los checkpoints.
   - **dataset_dir:** direccion de la carpeta compartida para el dataset.
5. **logfile:** archivo en el que se guardará el log del cliente.