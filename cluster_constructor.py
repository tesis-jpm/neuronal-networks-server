from threading import Lock
from config import Config

lock = Lock()
# holds and generate the cluster info
class ClusterConstructor:
    def __init__(self, chief_ip, chief_port) -> None:
        self.devices = {}
        self.chief_ip = chief_ip
        self.chief_port = chief_port
        self.cluster_header = None
        config = Config()
        self.default_ps_list = config.get("tensorflow.designated_ps")
        self.required_ps_number = config.get("tensorflow.max_ps")
        self.has_ps_workers = config.get("tensorflow.has_ps_workers")

    def get_devices_count(self):
        return len(self.devices)

    # add a device to the cluster
    # this is formatted like
    # {
    #   ip: 'device listening ip',
    #   port: 'device listening port',
    #   tensorflow_port1: 'first available port',
    #   tensorflow_port2: 'second available port'
    # }
    def add_device(self, device):
        with lock:
            self.devices[device["ip"]] = device

    # generates an array of cluster jsons
    # if a device ip is given it adds its task too
    # if not device ip given it returns the header directly (not an array)
    def generate_cluster_json(self, device_ip=None):
        # use locks to ensure only one thread generates the header
        with lock:
            # if its needed it generates the header
            if self.cluster_header is None:
                self.cluster_header = {
                    'worker': [],
                    'ps': [],
                    'chief': [f"{self.chief_ip}:{self.chief_port}"]
                }

                # add all parameter servers
                ps_counter = 0
                for ip in self.default_ps_list:
                    if ip in self.devices.keys():
                        self.add_ps(self.devices[ip], ps_counter)
                        ps_counter += 1

                for ip in self.devices:
                    if ps_counter >= self.required_ps_number:
                        break
                    if ip not in self.default_ps_list:
                        self.add_ps(self.devices[ip], ps_counter)
                        ps_counter += 1

                # add all workers
                worker_counter = 0
                for ip in self.devices:
                    device = self.devices[ip]
                    # ip port as worker
                    worker_ip_port = f'{device["ip"]}:{device["tensorflow_port1"]}'
                    # ip port as ps
                    ps_ip_port = f'{device["ip"]}:{device["tensorflow_port2"]}'
                    # if workers can be ps or this is not already a ps
                    if self.has_ps_workers or ps_ip_port not in self.cluster_header['ps']:
                        self.cluster_header['worker'].append(worker_ip_port)
                        self.add_task(ip, "worker", worker_counter)
                        worker_counter += 1
        if device_ip is not None:
            return {
                'cluster': self.cluster_header,
                'tasks': self.devices[device_ip]['tasks']
            }
        else:
            return {'cluster': self.cluster_header}

    def add_ps(self, device, ps_index):
        self.cluster_header['ps'].append(f'{device["ip"]}:{device["tensorflow_port2"]}')
        self.add_task(device["ip"], "ps", ps_index)

    def add_task(self, ip, task, index):
        if 'tasks' not in self.devices[ip]:
            self.devices[ip]['tasks'] = []
        self.devices[ip]['tasks'].append({
            'type': task,
            'index': index
        })
