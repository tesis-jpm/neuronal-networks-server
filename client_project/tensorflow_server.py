import tensorflow as tf
from config import Config

# is instantiated and gets to call start_server when the cluster has been prepared
class TensorflowServer:
    def __init__(self, cluster_conf) -> None:
        # has information about the cluster
        self.cluster_conf = cluster_conf
        # config with paths and configurations
        self.config = Config()


    def start_server(self, restart_callback):
        cluster_resolver = tf.distribute.cluster_resolver.TFConfigClusterResolver()
        # Run the coordinator.
        variable_partitioner = (
            tf.distribute.experimental.partitioners.MinSizePartitioner(
                min_shard_bytes=(256 << 10),
                max_shards=1  # cuantos ps hay
            )
        )

        strategy = tf.distribute.ParameterServerStrategy(
            cluster_resolver,
            variable_partitioner=variable_partitioner
        )

        with strategy.scope():
            # define and compile model here
            pass
        # model.fit here
