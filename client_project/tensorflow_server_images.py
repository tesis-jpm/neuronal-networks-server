import datetime
import json
import os
import pathlib

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import tensorflow as tf

from config import Config


# from IPython import display


class TensorflowServer:
    def __init__(self, cluster_conf) -> None:
        self.cluster_conf = cluster_conf
        self.config = Config()
        self.AUTOTUNE = tf.data.AUTOTUNE
        self.commands = None

    def decode_audio(self, audio_binary):
        # Decode WAV-encoded audio files to `float32` tensors, normalized
        # to the [-1.0, 1.0] range. Return `float32` audio and a sample rate.
        audio, _ = tf.audio.decode_wav(contents=audio_binary)
        # Since all the data is single channel (mono), drop the `channels`
        # axis from the array.
        return tf.squeeze(audio, axis=-1)

    def get_label(self, file_path):
        parts = tf.strings.split(
            input=file_path,
            sep=os.path.sep)
        # Note: You'll use indexing here instead of tuple unpacking to enable this
        # to work in a TensorFlow graph.
        return parts[-2]

    def get_waveform_and_label(self, file_path):
        label = self.get_label(file_path)
        audio_binary = tf.io.read_file(file_path)
        waveform = self.decode_audio(audio_binary)
        return waveform, label

    def get_spectrogram(self, waveform):
        # Zero-padding for an audio waveform with less than 16,000 samples.
        input_len = 16000
        waveform = waveform[:input_len]
        zero_padding = tf.zeros(
            [16000] - tf.shape(waveform),
            dtype=tf.float32)
        # Cast the waveform tensors' dtype to float32.
        waveform = tf.cast(waveform, dtype=tf.float32)
        # Concatenate the waveform with `zero_padding`, which ensures all audio
        # clips are of the same length.
        equal_length = tf.concat([waveform, zero_padding], 0)
        # Convert the waveform to a spectrogram via a STFT.
        spectrogram = tf.signal.stft(
            equal_length, frame_length=255, frame_step=128)
        # Obtain the magnitude of the STFT.
        spectrogram = tf.abs(spectrogram)
        # Add a `channels` dimension, so that the spectrogram can be used
        # as image-like input data with convolution layers (which expect
        # shape (`batch_size`, `height`, `width`, `channels`).
        spectrogram = spectrogram[..., tf.newaxis]
        return spectrogram

    def plot_spectrogram(self, spectrogram, ax):
        if len(spectrogram.shape) > 2:
            assert len(spectrogram.shape) == 3
            spectrogram = np.squeeze(spectrogram, axis=-1)
        # Convert the frequencies to log scale and transpose, so that the time is
        # represented on the x-axis (columns).
        # Add an epsilon to avoid taking a log of zero.
        log_spec = np.log(spectrogram.T + np.finfo(float).eps)
        height = log_spec.shape[0]
        width = log_spec.shape[1]
        x = np.linspace(0, np.size(spectrogram), num=width, dtype=int)
        y = range(height)
        ax.pcolormesh(x, y, log_spec)

    def get_spectrogram_and_label_id(self, audio, label):
        spectrogram = self.get_spectrogram(audio)
        label_id = tf.argmax(label == self.commands)
        return spectrogram, label_id

    def preprocess_dataset(self, files):
        files_ds = tf.data.Dataset.from_tensor_slices(files)
        output_ds = files_ds.map(
            map_func=self.get_waveform_and_label,
            num_parallel_calls=self.AUTOTUNE)
        output_ds = output_ds.map(
            map_func=self.get_spectrogram_and_label_id,
            num_parallel_calls=self.AUTOTUNE)
        return output_ds.repeat()

    def start_server(self, restart_callback):
        cluster_resolver = tf.distribute.cluster_resolver.TFConfigClusterResolver()

        # Run the coordinator.
        variable_partitioner = (
            tf.distribute.experimental.partitioners.MinSizePartitioner(
                min_shard_bytes=(256 << 10),
                max_shards=1  # cuantos ps hay
            )
        )

        strategy = tf.distribute.ParameterServerStrategy(
            cluster_resolver,
            variable_partitioner=variable_partitioner
        )

        # Set the seed value for experiment reproducibility.
        global input_shape
        seed = 42
        tf.random.set_seed(seed)
        np.random.seed(seed)
        database_path = self.config.get("paths.dataset_dir")
        data_dir = pathlib.Path(database_path)

        self.commands = np.array(tf.io.gfile.listdir(str(data_dir)))
        self.commands = self.commands[self.commands != 'README.md']
        print('Commands:', self.commands)

        filenames = tf.io.gfile.glob(str(data_dir) + '/*/*')
        filenames = tf.random.shuffle(filenames)
        num_samples = len(filenames)
        print('Number of total examples:', num_samples)
        print('Number of examples per label:',
              len(tf.io.gfile.listdir(str(data_dir / self.commands[0]))))
        print('Example file tensor:', filenames[0])

        train_files = filenames[:105000]
        val_files = filenames[105000: 105000 + 800]
        test_files = filenames[-800:]

        print('Training set size', len(train_files))
        print('Validation set size', len(val_files))
        print('Test set size', len(test_files))

        test_file = tf.io.read_file(database_path + '/down/0a9f9af7_nohash_0.wav')
        test_audio, _ = tf.audio.decode_wav(contents=test_file)

        files_ds = tf.data.Dataset.from_tensor_slices(train_files).repeat()

        waveform_ds = files_ds.map(
            map_func=self.get_waveform_and_label,
            num_parallel_calls=self.AUTOTUNE)

        spectrogram_ds = waveform_ds.map(
            map_func=self.get_spectrogram_and_label_id,
            num_parallel_calls=self.AUTOTUNE)

        for spectrogram, _ in spectrogram_ds.take(1):
            input_shape = spectrogram.shape
        print('Input shape:', input_shape)
        num_labels = len(self.commands)

        print("Start strategy.scope")
        with strategy.scope():
            # Instantiate the `tf.keras.layers.Normalization` layer.
            norm_layer = tf.keras.layers.Normalization()
            # Fit the state of the layer to the spectrograms
            # with `Normalization.adapt`.
            norm_layer.adapt(data=spectrogram_ds.map(map_func=lambda spec, label: spec), steps=100)

            model = tf.keras.models.Sequential([
                tf.keras.layers.Input(shape=input_shape),
                # Downsample the input.
                tf.keras.layers.Resizing(32, 32),
                # Normalize.
                norm_layer,
                tf.keras.layers.Conv2D(32, 3, activation='relu'),
                tf.keras.layers.Conv2D(64, 3, activation='relu'),
                tf.keras.layers.MaxPooling2D(),
                tf.keras.layers.Dropout(0.25),
                tf.keras.layers.Flatten(),
                tf.keras.layers.Dense(128, activation='relu'),
                tf.keras.layers.Dropout(0.5),
                tf.keras.layers.Dense(num_labels),
            ])

            model.summary()

            print("Start compile")
            model.compile(
                optimizer=tf.keras.optimizers.legacy.Adam(),
                loss=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
                metrics=['accuracy'],
            )

            # Define the checkpoint directory to store the checkpoints.
            checkpoint_dir = self.config.get("paths.checkpoint_dir")
            # Define the name of the checkpoint files.
            checkpoint_prefix = os.path.join(checkpoint_dir, "ckpt_{epoch}")
            # Put all the callbacks together.
            checkpoint_callback = tf.keras.callbacks.ModelCheckpoint(
                filepath=checkpoint_prefix,
                save_weights_only=True
            )
            print("spectrogram_ds")

            train_ds = spectrogram_ds
            val_ds = self.preprocess_dataset(val_files)

            batch_size = 64
            train_ds = train_ds.batch(batch_size)
            val_ds = val_ds.batch(batch_size)

            # train_ds = train_ds.cache().prefetch(self.AUTOTUNE)
            # val_ds = val_ds.cache().prefetch(self.AUTOTUNE)



        print("Start model.fit")
        EPOCHS = 3
        history = model.fit(
            train_ds,
            validation_data=val_ds,
            steps_per_epoch=2350,
            #steps_per_epoch=10,
            validation_steps=15,
            epochs=EPOCHS,
            callbacks=[
                tf.keras.callbacks.BackupAndRestore(checkpoint_dir),
                tf.keras.callbacks.EarlyStopping(verbose=1, patience=2),
                restart_callback
            ],
        )

        model.save("\\\\10.1.1.22\\dataset\\save")
        #metrics = history.history
        #plt.plot(history.epoch, metrics['loss'], metrics['val_loss'])
        #plt.legend(['loss', 'val_loss'])
        #plt.show()
