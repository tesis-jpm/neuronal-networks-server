import os
import socket as socket_lib
import json
import threading
from time import sleep
from datetime import datetime
from multiprocessing import Process
import shutil
import logging

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import tensorflow as tf
from tensorflow.python.framework.errors_impl import UnknownError

from cluster_constructor import ClusterConstructor
from client_project.tensorflow_server import TensorflowServer
from config import Config
from mailer import Mailer

config = Config()
mailer = Mailer()

print_lock = threading.Lock()
cancel_lock = threading.Lock()
logging.basicConfig(filename=config.get("logfile"), encoding='utf-8', level=logging.INFO)


def print_with_lock(msg):
    with print_lock:
        print(msg)
        logging.info(msg)


class Server:
    def __init__(self):
        self.main_network = None
        self.process = None
        self.listening_thread = None
        self.available_interfaces = []
        self.cluster_constructor = None

    def start(self):
        # get server ip
        interfaces = socket_lib.getaddrinfo(host=socket_lib.gethostname(), port=None, family=socket_lib.AF_INET)
        all_ips = [ip[-1][0] for ip in interfaces]

        # # send broadcast
        for ip in all_ips:
            # get available port
            listening_socket = socket_lib.socket(socket_lib.AF_INET, socket_lib.SOCK_DGRAM)
            listening_socket.setsockopt(socket_lib.SOL_SOCKET, socket_lib.SO_BROADCAST, 1)
            listening_socket.bind((ip, 0))
            listening_port = listening_socket.getsockname()[1]
            self.available_interfaces.append((ip, listening_port))
            # create a cluster constructor
            self.cluster_constructor = ClusterConstructor(ip, listening_port)
            # start to listen in a new thread
            self.process = Process(
                target=self.wait_for_clients,
                args=(listening_socket, self.cluster_constructor,)
            )
            self.process.start()
            # send broadcast
            print_with_lock(f"Waiting for clients.")
            msg = {"ip": ip, "port": listening_port}
            print_with_lock(f"Server information: {msg}")
            data = json.dumps(msg)
            self.send_broadcast(ip, data)

        sleep(config.get("broadcast.wait_time_seconds"))
        self.stop_listening()

    def send_broadcast(self, ip, message):
        socket = socket_lib.socket(socket_lib.AF_INET, socket_lib.SOCK_DGRAM, socket_lib.IPPROTO_UDP)  # UDP
        socket.setsockopt(socket_lib.SOL_SOCKET, socket_lib.SO_BROADCAST, 1)
        socket.bind((ip, 0))
        socket.sendto(message.encode('utf-8'), ("255.255.255.255", config.get("broadcast.port")))
        socket.close()

    def stop_listening(self):
        for listening_port in self.available_interfaces:
            socket = socket_lib.socket(socket_lib.AF_INET, socket_lib.SOCK_DGRAM)  # UDP
            socket.sendto('{"stop":1}'.encode('utf-8'), listening_port)
            socket.close()

    def restart_server(self, ip=None, should_kill=True):
        if should_kill:
            self.process.kill()
        if ip is None:
            ip = self.cluster_constructor.chief_ip
        data = json.dumps({"restart": 1})
        self.send_broadcast(ip, data)
        sleep(config.get("broadcast.wait_time_seconds"))
        self.start()

    def stop_training(self, ip):
        self.process.kill()
        data = json.dumps({"stop": 1})
        self.send_broadcast(ip, data)

    def wait_for_clients(self, socket, cluster_constructor):
        # intern messages
        stop_message = '{"stop":1}'.encode('utf-8')

        keep_listening = True
        print_with_lock(f"Waiting for clients connection.")
        while keep_listening and \
                (self.main_network == cluster_constructor.chief_ip or self.main_network is None):
            received_data = socket.recvfrom(1024)[0]
            if received_data == stop_message:
                print_with_lock(f"The process of waiting for clients has finished.")
                keep_listening = False
            else:
                # if it was a response from a client check if there is already a main_network
                # if there is not, it is set as the main_network
                with cancel_lock:
                    if self.main_network is None:
                        self.main_network = cluster_constructor.chief_ip

                if self.main_network == cluster_constructor.chief_ip:
                    thread = threading.Thread(target=self.manage_response, args=(received_data, cluster_constructor))
                    thread.start()
        socket.close()

        if self.main_network != cluster_constructor.chief_ip:
            return
        if cluster_constructor.get_devices_count() > 0:
            self.send_tasks(cluster_constructor)
            self.process = Process(
                target=self.start_training,
                args=(cluster_constructor,)
            )
            self.process.start()
            self.process.join()
        else:
            print(f"{datetime.now()} The training was stopped due to lack of devices.")
            mailer.send_email("The training was stopped due to lack of devices.", False)

    def manage_response(self, received_data, cluster_constructor):
        json_data = json.loads(received_data.decode("utf-8"))
        client_ip = json_data["ip"]
        cluster_constructor.add_device(json_data)
        print_with_lock(f"- Response from worker {client_ip} received.")

    def send_tasks(self, cluster_constructor):
        threads = []
        # send data to clients in different threads
        for ip in cluster_constructor.devices:
            device = cluster_constructor.devices[ip]
            thread = threading.Thread(target=self.send_data_to_worker, args=(device, cluster_constructor,))
            threads.append(thread)
            thread.start()
        # wait for all the data to be sent
        for thread in threads:
            thread.join()
        print_with_lock(f"Cluster data sent to all devices.")
        cluster_json = cluster_constructor.generate_cluster_json()
        info_msg = f"Number of devices: {cluster_constructor.get_devices_count()}\ncluster_data: {cluster_json}"
        print_with_lock(info_msg)
        mailer.send_email(info_msg, False)

    def send_data_to_worker(self, device, cluster_constructor):
        # send data to worker
        msg = cluster_constructor.generate_cluster_json(device['ip'])
        data = json.dumps(msg)
        socket = socket_lib.socket(socket_lib.AF_INET, socket_lib.SOCK_DGRAM)
        socket.sendto(data.encode('utf-8'), (device['ip'], device['port']))

        socket.close()

    def start_training(self, cluster_constructor):
        print_with_lock(f"{datetime.now()} The training has started.")
        mailer.send_email("The training has started.", False)

        os.environ["grpc_fail_fast"] = "use_caller"
        os.environ["GRPC_FAIL_FAST"] = "use_caller"
        cluster_conf = cluster_constructor.generate_cluster_json()
        cluster_conf_new = {"cluster": cluster_conf.get("cluster"),
                            "task": {'type': 'chief', 'index': 0}}
        tensorflow_server = TensorflowServer(cluster_conf_new)

        os.environ["TF_CONFIG"] = json.dumps(cluster_conf_new)
        # Set the environment variable to allow reporting worker and ps failure to the
        # coordinator. This is a workaround and won't be necessary in the future.
        os.environ["GRPC_FAIL_FAST"] = "use_caller"

        try:
            tensorflow_server.start_server(RestartTrainingCallback(self.restart_server))
            print_with_lock(f"{datetime.now()} Training finished.")
            mailer.send_email(f"Training finished.", False)
        except UnknownError:
            print_with_lock(f"{datetime.now()} An error has ocurred and training has stopped, please check the logfile for more details.")
            mailer.send_email(f"An error has ocurred and training has stopped, please check the logfile for more details.", False)
            self.restart_server(cluster_constructor.chief_ip, False)
        data = json.dumps({"restart": 1})
        self.send_broadcast(cluster_constructor.chief_ip, data)

    def copy_project_folder(self):
        curr_dir = os.getcwd()

        file_source = config.get("paths.client_project_dir")
        file_destination = curr_dir + '\\client_project\\'

        get_files = os.listdir(file_source)

        for g in get_files:
            shutil.copy(file_source + g, file_destination)

    def clear_log(self):
        path = os.getcwd() + '\\info.log'

        try:
            open(path, 'w').close()
        except IOError:
            print('Failure')


class RestartTrainingCallback(tf.keras.callbacks.Callback):
    def __init__(self, restart_function):
        self.restart_function = restart_function
        self.count = 0

    def on_epoch_begin(self, epoch, logs=None):
        if self.count >= 1:
            self.restart_function(None, False)
        self.count += 1
        mailer.send_email(f"Epoch #{epoch + 1} begins.", True)
        print_with_lock(f"{datetime.now()} Epoch #{epoch + 1} begins.")

    def on_epoch_end(self, epoch, logs=None):
        if logs is None:
            logs = {}
        print_with_lock(f"{datetime.now()} Epoch #{epoch + 1} ends.")
        msg = "%s Epoch #%i resume, %s" % (datetime.now(), epoch + 1, ", ".join("%s: %f" % (k, v) for k, v in logs.items()))
        logging.info(msg)



if __name__ == "__main__":
    server = Server()
    server.clear_log()
    server.copy_project_folder()
    server.start()
