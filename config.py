import json


class Config:
    # Initialize the Config class preloading the data from config.json
    is_initialized = False
    options = None
    def __init__(self):
        if not self.is_initialized:
            f = open("./config.json", "r")
            s_config = f.read()
            f.close()
            self.options = json.loads(s_config)
            self.is_initialized = True

    # gets a specific configuration
    # if the configuration is nested, ex: { server: { port: 8080 } }
    # it has to be accessed with dots, ex: server.port
    def get(self, config_name):
        split_config_dir = config_name.split(".")
        temp_dic = self.options
        for item in split_config_dir:
            temp_dic = temp_dic[item]
        return temp_dic
