import smtplib
from email.mime.text import MIMEText
from email.mime.multipart import MIMEMultipart
from config import Config


class Mailer:

    def __init__(self):
        config = Config()
        self.sender = config.get("mailer.email")
        self.sender_password = config.get("mailer.password")
        self.receiver = config.get("mailer.receiver")
        self.enabled = config.get("mailer.enabled")
        self.enabled_epoch = config.get("mailer.enabled_epoch")
        self.smtp_server = config.get("mailer.smtp_server")
        self.smtp_port = config.get("mailer.smtp_port")
        self.mail_header = config.get("mailer.header")

    def send_email(self, text, is_epoch):
        if (self.enabled and not is_epoch) or (self.enabled_epoch and is_epoch):
            # Send the message
            with smtplib.SMTP(self.smtp_server, self.smtp_port) as smtp:
                smtp.starttls()
                smtp.login(self.sender, self.sender_password)
                message = self.build_message(text)
                smtp.send_message(message)
                smtp.quit()
                print(f"Email sent: {message}")

    def build_message(self, text):
        message = MIMEMultipart()
        message["From"] = self.sender
        message["To"] = self.receiver
        message["Subject"] = self.mail_header
        body = text
        message.attach(MIMEText(body, "plain"))
        return message
